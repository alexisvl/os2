# Character set, as importable Python

# Trigger markers. Encoding allows simple logic:
# bits = 0001bbaa
#
#   bb, aa = 01 if trigger enabled but not trig'd
#   bb, aa = 11 if trigger enabled and trig'd
CH_TRIG_a           = 0x12
CH_TRIG_A           = 0x13
CH_TRIG_b           = 0x18
CH_TRIG_ab          = 0x1A
CH_TRIG_Ab          = 0x1B
CH_TRIG_B           = 0x1C
CH_TRIG_aB          = 0x1E
CH_TRIG_AB          = 0x1F

CH_INTEN_LOWER_A    = 0x20
CH_INTEN_LOWER_B    = 0x21
CH_INTEN_LOWER_C    = 0x22
CH_INTEN_LOWER_D    = 0x23
CH_INTEN_LOWER_E    = 0x24
CH_INTEN_LOWER_G    = 0x25
CH_INTEN_LOWER_H    = 0x26
CH_INTEN_LOWER_I    = 0x27
CH_INTEN_LOWER_J    = 0x28
CH_INTEN_LOWER_L    = 0x29
CH_INTEN_LOWER_O    = 0x2A
CH_INTEN_LOWER_Q    = 0x2B
CH_INTEN_LOWER_R    = 0x2C
CH_INTEN_LOWER_T    = 0x2D
CH_INTEN_LOWER_U    = 0x2E
CH_INTEN_LOWER_V    = 0x2F

CH_INTEN_LOWER_W    = 0x30
CH_INTEN_LOWER_X    = 0x31
CH_INTEN_LOWER_Y    = 0x32
CH_INTEN_LOWER_Z    = 0x33
CH_INTEN_LOWER_ALPHA    = 0x34
CH_INTEN_LOWER_BETA     = 0x35
CH_INTEN_LOWER_DELTA    = 0x36
CH_INTEN_DERIV_DELTA    = 0x37
CH_INTEN_LOWER_THETA    = 0x38
CH_INTEN_LOWER_LAMBDA   = 0x39
CH_INTEN_LOWER_PI       = 0x3A
CH_INTEN_LOWER_TAU      = 0x3B
CH_INTEN_LOWER_PHI      = 0x3C
CH_INTEN_LOEWR_OMEGA    = 0x3D
CH_INTEN_FUNCTION       = 0x3E
CH_INTEN_INTEGRAL       = 0x3F

CH_INTEN_0          = 0x40
CH_INTEN_1          = 0x41
CH_INTEN_2          = 0x42
CH_INTEN_3          = 0x43
CH_INTEN_4          = 0x44
CH_INTEN_5          = 0x45
CH_INTEN_6          = 0x46
CH_INTEN_7          = 0x47
CH_INTEN_8          = 0x48
CH_INTEN_9          = 0x49
CH_INTEN_UPPER_A    = 0x4A
CH_INTEN_UPPER_B    = 0x4B
CH_INTEN_UPPER_C    = 0x4C
CH_INTEN_UPPER_D    = 0x4D
CH_INTEN_UPPER_E    = 0x4E
CH_INTEN_UPPER_F    = 0x4F

CH_INTEN_UPPER_G    = 0x50
CH_INTEN_UPPER_H    = 0x51
CH_INTEN_UPPER_I    = 0x52
CH_INTEN_UPPER_J    = 0x53
CH_INTEN_UPPER_K    = 0x54
CH_INTEN_UPPER_L    = 0x55
CH_INTEN_UPPER_M    = 0x56
CH_INTEN_UPPER_N    = 0x57
CH_INTEN_UPPER_O    = 0x58
CH_INTEN_UPPER_P    = 0x59
CH_INTEN_UPPER_Q    = 0x5A
CH_INTEN_UPPER_R    = 0x5B
CH_INTEN_UPPER_S    = 0x5C
CH_INTEN_UPPER_T    = 0x5D
CH_INTEN_UPPER_U    = 0x5E
CH_INTEN_UPPER_V    = 0x5F

CH_INTEN_UPPER_W    = 0x60
CH_INTEN_UPPER_X    = 0x61
CH_INTEN_UPPER_Y    = 0x62
CH_INTEN_UPPER_Z    = 0x63
CH_INTEN_UPPER_DELTA = 0x64
CH_INTEN_LOWER_F    = 0x65
CH_INTEN_LOWER_K    = 0x66
CH_INTEN_LOWER_M    = 0x67
CH_INTEN_LOWER_N    = 0x68
CH_INTEN_LOWER_P    = 0x69
CH_INTEN_LOWER_S    = 0x6A
CH_INTEN_LOWER_MU   = 0x6B
CH_INTEN_UPPER_OMEGA = 0x6C
CH_INTEN_DEGREE     = 0x6D
CH_INTEN_LESS       = 0x6E
CH_INTEN_GREATER    = 0x6F

CH_UPPER_GAMMA      = 0x70
CH_UPPER_SIGMA      = 0x71
CH_LOWER_SIGMA      = 0x72
CH_UPPER_PHI        = 0x73
CH_UPPER_THETA      = 0x74
CH_INFINITY         = 0x75
CH_LOWER_EPSILON    = 0x76
CH_AT               = 0x77
# 0x78
# 0x79
# 0x7A
# 0x7B
CH_SMILE            = 0x7C
CH_FROWN            = 0x7D
CH_HEART            = 0x7E
CH_50BLOCK          = 0x7F

CH_BLOCK    = 0x80
CH_EXCLAM   = 0x81
CH_HASH     = 0x82
CH_PERCENT  = 0x83
CH_AMPER    = 0x84
CH_L_AND    = 0x85
CH_L_OR     = 0x86
CH_L_XOR    = 0x87
CH_L_NOT    = 0x88
CH_PAREN_L  = 0x89
CH_PAREN_R  = 0x8A
CH_SQUOTE   = 0x8B
CH_DQUOTE   = 0x8C
CH_TR_RISE  = 0x8D
CH_TR_FALL  = 0x8E
CH_TR_BOTH  = 0x8F

CH_MINUS    = 0x90
CH_PLUS     = 0x91
CH_PLUSMINUS= 0x92
CH_TIMES    = 0x93
CH_DIVIDE   = 0x94
CH_LESSEQ   = 0x95
CH_EQUAL    = 0x96
CH_GREATEQ  = 0x97
CH_NOTEQUAL = 0x98
CH_LEFT     = 0x99
CH_RIGHT    = 0x9A
CH_UP       = 0x9B
CH_BACKSLS  = 0x9C
CH_FWDSLS   = 0x9D
CH_COMMA    = 0x9E
CH_UNDERSCR = 0x9F

CH_LOWER_A  = 0xA0
CH_LOWER_B  = 0xA1
CH_LOWER_C  = 0xA2
CH_LOWER_D  = 0xA3
CH_LOWER_E  = 0xA4
CH_LOWER_G  = 0xA5
CH_LOWER_H  = 0xA6
CH_LOWER_I  = 0xA7
CH_LOWER_J  = 0xA8
CH_LOWER_L  = 0xA9
CH_LOWER_O  = 0xAA
CH_LOWER_Q  = 0xAB
CH_LOWER_R  = 0xAC
CH_LOWER_T  = 0xAD
CH_LOWER_U  = 0xAE
CH_LOWER_V  = 0xAF

CH_LOWER_W      = 0xB0
CH_LOWER_X      = 0xB1
CH_LOWER_Y      = 0xB2
CH_LOWER_Z      = 0xB3
CH_LOWER_ALPHA  = 0xB4
CH_LOWER_BETA   = 0xB5
CH_LOWER_DELTA  = 0xB6
CH_DERIV_DELTA  = 0xB7
CH_LOWER_THETA  = 0xB8
CH_LOWER_LAMBDA = 0xB9
CH_LOWER_PI     = 0xBA
CH_LOWER_TAU    = 0xBB
CH_LOWER_PHI    = 0xBC
CH_LOWER_OMEGA  = 0xBD
CH_FUNCTION     = 0xBE
CH_INTEGRAL     = 0xBF

CH_0        = 0xC0
CH_1        = 0xC1
CH_2        = 0xC2
CH_3        = 0xC3
CH_4        = 0xC4
CH_5        = 0xC5
CH_6        = 0xC6
CH_7        = 0xC7
CH_8        = 0xC8
CH_9        = 0xC9
CH_UPPER_A  = 0xCA
CH_UPPER_B  = 0xCB
CH_UPPER_C  = 0xCC
CH_UPPER_D  = 0xCD
CH_UPPER_E  = 0xCE
CH_UPPER_F  = 0xCF

CH_UPPER_G  = 0xD0
CH_UPPER_H  = 0xD1
CH_UPPER_I  = 0xD2
CH_UPPER_J  = 0xD3
CH_UPPER_K  = 0xD4
CH_UPPER_L  = 0xD5
CH_UPPER_M  = 0xD6
CH_UPPER_N  = 0xD7
CH_UPPER_O  = 0xD8
CH_UPPER_P  = 0xD9
CH_UPPER_Q  = 0xDA
CH_UPPER_R  = 0xDB
CH_UPPER_S  = 0xDC
CH_UPPER_T  = 0xDD
CH_UPPER_U  = 0xDE
CH_UPPER_V  = 0xDF

CH_UPPER_W      = 0xE0
CH_UPPER_X      = 0xE1
CH_UPPER_Y      = 0xE2
CH_UPPER_Z      = 0xE3
CH_UPPER_DELTA  = 0xE4
CH_LOWER_F      = 0xE5
CH_LOWER_K      = 0xE6
CH_LOWER_M      = 0xE7
CH_LOWER_N      = 0xE8
CH_LOWER_P      = 0xE9
CH_LOWER_S      = 0xEA
CH_LOWER_MU     = 0xEB
CH_UPPER_OMEGA  = 0xEC
CH_DEGREE       = 0xED
CH_LESS         = 0xEE
CH_GREATER      = 0xEF

CH_DOWN         = 0xF0
CH_SQUARED      = 0xF1
CH_CARET        = 0xF2
CH_RADICAL      = 0xF3
CH_CAP          = 0xF4
CH_GROUND       = 0xF5
CH_BW           = 0xF6
CH_SELECT_EMPTY = 0xF7
CH_SELECT_SOLID = 0xF8

CH_DB           = 0xF9
CH_AC_A         = 0xFA
CH_AC_V         = 0xFB
CH_SOURCE_X     = 0xFC
CH_QUESTION     = 0xFD
CH_PERIOD       = 0xFE
CH_SPACE        = 0xFF


mapping = [
    ("0",   CH_0),
    ("1",   CH_1),
    ("2",   CH_2),
    ("3",   CH_3),
    ("4",   CH_4),
    ("5",   CH_5),
    ("6",   CH_6),
    ("7",   CH_7),
    ("8",   CH_8),
    ("9",   CH_9),

    ("A",   CH_UPPER_A),
    ("B",   CH_UPPER_B),
    ("C",   CH_UPPER_C),
    ("D",   CH_UPPER_D),
    ("E",   CH_UPPER_E),
    ("F",   CH_UPPER_F),
    ("G",   CH_UPPER_G),
    ("H",   CH_UPPER_H),
    ("I",   CH_UPPER_I),
    ("J",   CH_UPPER_J),
    ("K",   CH_UPPER_K),
    ("L",   CH_UPPER_L),
    ("M",   CH_UPPER_M),
    ("N",   CH_UPPER_N),
    ("O",   CH_UPPER_O),
    ("P",   CH_UPPER_P),
    ("Q",   CH_UPPER_Q),
    ("R",   CH_UPPER_R),
    ("S",   CH_UPPER_S),
    ("T",   CH_UPPER_T),
    ("U",   CH_UPPER_U),
    ("V",   CH_UPPER_V),
    ("W",   CH_UPPER_W),
    ("X",   CH_UPPER_X),
    ("Y",   CH_UPPER_Y),
    ("Z",   CH_UPPER_Z),

    ("a",   CH_LOWER_A),
    ("b",   CH_LOWER_B),
    ("c",   CH_LOWER_C),
    ("d",   CH_LOWER_D),
    ("e",   CH_LOWER_E),
    ("f",   CH_LOWER_F),
    ("g",   CH_LOWER_G),
    ("h",   CH_LOWER_H),
    ("i",   CH_LOWER_I),
    ("j",   CH_LOWER_J),
    ("k",   CH_LOWER_K),
    ("l",   CH_LOWER_L),
    ("m",   CH_LOWER_M),
    ("n",   CH_LOWER_N),
    ("o",   CH_LOWER_O),
    ("p",   CH_LOWER_P),
    ("q",   CH_LOWER_Q),
    ("r",   CH_LOWER_R),
    ("s",   CH_LOWER_S),
    ("t",   CH_LOWER_T),
    ("u",   CH_LOWER_U),
    ("v",   CH_LOWER_V),
    ("w",   CH_LOWER_W),
    ("x",   CH_LOWER_X),
    ("y",   CH_LOWER_Y),
    ("z",   CH_LOWER_Z),

    (".",       CH_PERIOD),
    (",",       CH_COMMA),
    ("?",       CH_QUESTION),
    ("!",       CH_EXCLAM),
    ("#",       CH_HASH),
    ("%",       CH_PERCENT),
    ("&",       CH_AMPER),
    ("{and}",   CH_L_AND),
    ("{or}",    CH_L_OR),
    ("{xor}",   CH_L_XOR),
    ("{not}",   CH_L_NOT),
    ("(",       CH_PAREN_L),
    (")",       CH_PAREN_R),
    ("'",       CH_SQUOTE),
    ("\"",      CH_DQUOTE),
    ("{rise}",  CH_TR_RISE),
    ("{fall}",  CH_TR_FALL),
    ("{both}",  CH_TR_BOTH),

    ("-",       CH_MINUS),
    ("+",       CH_PLUS),
    ("±",       CH_PLUSMINUS),
    ("×",       CH_TIMES),
    ("÷",       CH_DIVIDE),
    ("<",       CH_LESS),
    ("≤",       CH_LESSEQ),
    ("=",       CH_EQUAL),
    ("≥",       CH_GREATEQ),
    (">",       CH_GREATER),
    ("≠",       CH_NOTEQUAL),

    ("{left}",  CH_LEFT),
    ("{right}", CH_RIGHT),
    ("{up}",    CH_UP),
    ("{down}",  CH_DOWN),
    ("\\",      CH_BACKSLS),
    ("/",       CH_FWDSLS),
    ("_",       CH_UNDERSCR),
    ("@",       CH_AT),
    ("{infinity}", CH_INFINITY),

    ("{alpha}", CH_LOWER_ALPHA),
    ("{beta}",  CH_LOWER_BETA),
    ("{Delta}", CH_UPPER_DELTA),
    ("{delta}", CH_LOWER_DELTA),
    ("{deriv}", CH_DERIV_DELTA),
    ("{theta}", CH_LOWER_THETA),
    ("{lambda}",CH_LOWER_LAMBDA),
    ("{mu}",    CH_LOWER_MU),
    ("µ",       CH_LOWER_MU),
    ("{pi}",    CH_LOWER_PI),
    ("{tau}",   CH_LOWER_TAU),
    ("{phi}",   CH_LOWER_PHI),
    ("{Omega}", CH_UPPER_OMEGA),
    ("{omega}", CH_LOWER_OMEGA),
    ("{Gamma}", CH_UPPER_GAMMA),
    ("{Sigma}", CH_UPPER_SIGMA),
    ("{sigma}", CH_LOWER_SIGMA),
    ("{Phi}",   CH_UPPER_PHI),
    ("{Theta}", CH_UPPER_THETA),
    ("{epsilon}", CH_LOWER_EPSILON),


    ("°",       CH_DEGREE),
    ("{func}",  CH_FUNCTION),
    ("{intg}",  CH_INTEGRAL),
    ("²",       CH_SQUARED),
    ("^",       CH_CARET),
    ("{root}",  CH_RADICAL),
    ("{cap}",   CH_CAP),
    ("{gnd}",   CH_GROUND),
    ("{bw}",    CH_BW),
    ("{selemp}",CH_SELECT_EMPTY),
    ("{selsol}",CH_SELECT_SOLID),
    ("{db}",    CH_DB),
    ("{acA}",   CH_AC_A),
    ("{acV}",   CH_AC_V),
    ("{x}",     CH_SOURCE_X),
    (" ",       CH_SPACE),

    ("{:)}",    CH_SMILE),
    ("{:(}",    CH_FROWN),
    ("{<3}",    CH_HEART),
    ("{50block}", CH_50BLOCK),
    ("{block}", CH_BLOCK),

    ("{triga}",  CH_TRIG_a),
    ("{trigA}",  CH_TRIG_A),
    ("{trigb}",  CH_TRIG_b),
    ("{trigB}",  CH_TRIG_B),
    ("{trigab}", CH_TRIG_ab),
    ("{trigaB}", CH_TRIG_aB),
    ("{trigAb}", CH_TRIG_Ab),
    ("{trigAB}", CH_TRIG_AB),
]

def encode(s, _memo={}):
    if s in _memo:
        return _memo[s]

    out = []
    i = 0
    while i < len(s):
        for mnemonic, value in mapping:
            if s.startswith(mnemonic, i):
                out.append(value)
                i += len(mnemonic)
                break
        else:
            raise ValueError(f"cannot convert {s[i]!r}")
    enc = bytes(out)
    _memo[s] = enc
    return enc
