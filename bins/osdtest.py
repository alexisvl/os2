# OSDTEST Implementation
# Copyright (C) 2020 Alexis Lockwood, <alexlockwood@fastmail.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

import eeplut
from resources import charset

import functools

@eeplut.auto
class OsdTest(eeplut.Eeplut):
    def __init__(self):
        super().__init__(size_kbits = 512, width_bits = 8)

    def logic_functions(self):
        return (
            [self.fn_default]
            + [functools.partial(self.fn_field, i) for i in range(16)]
        )

    def inputs_map(self):
        return {
            0: "POS0",
            1: "POS1",
            2: "POS2",

            3: "FLD0",
            4: "FLD1",
            5: "FLD2",
            6: "FLD3",

            7: "PAGE0",
            8: "PAGE1",
            9: "PAGE2",
            10: "PAGE3",
            11: "PAGE4",
            12: "PAGE5",
            13: "PAGE6",
            14: "PAGE7",
            15: "PAGE8",
        }

    def outputs_map(self):
        return {
            "CHAR0": 0,
            "CHAR1": 1,
            "CHAR2": 2,
            "CHAR3": 3,
            "CHAR4": 4,
            "CHAR5": 5,
            "CHAR6": 6,
            "CHAR7": 7,
        }

    def fn_default(self, inputs):
        return {
            "CHAR0": "H",
            "CHAR1": "H",
            "CHAR2": "H",
            "CHAR3": "H",
            "CHAR4": "H",
            "CHAR5": "H",
            "CHAR6": "H",
            "CHAR7": "H",
        }

    def fn_field(self, n, inputs):
        fld = eeplut.concat(inputs, "FLD3", "FLD2", "FLD1", "FLD0")

        if fld != n:
            return {}

        pos = eeplut.concat(inputs, "POS2", "POS1", "POS0")

        page = eeplut.concat(
            inputs,
            "PAGE8", "PAGE7", "PAGE6", "PAGE5", "PAGE4",
            "PAGE3", "PAGE2", "PAGE1", "PAGE0",
        )

        page_manual = (page & 0x1F0) >> 4
        page_auto = page & 0xF

        fields = [""] * 16
        fields_enc = None

        if page_manual == 0:
            # Character set demo
            fields_enc = []
            if (page_auto % 2) == 0:
                for i in range(16):
                    fields_enc.append(bytes(range(i * 8, i * 8 + 8)))
            else:
                for i in range(16):
                    fields_enc.append(bytes(range(i * 8 + 128, i * 8 + 128 + 8)))

        elif page_manual == 1:
            # Field numbers
            fields[0o00] = "FIELD00"
            fields[0o01] = "FIELD01"
            fields[0o02] = "FIELD02"
            fields[0o03] = "FIELD03"
            fields[0o04] = "FIELD04"
            fields[0o05] = "FIELD05"
            fields[0o06] = "FIELD06"
            fields[0o07] = "FIELD07"
            fields[0o10] = "FIELD10"
            fields[0o11] = "FIELD11"
            fields[0o12] = "FIELD12"
            fields[0o13] = "FIELD13"
            fields[0o14] = "FIELD14"
            fields[0o15] = "FIELD15"
            fields[0o16] = "FIELD16"
            fields[0o17] = "FIELD17"

        elif page_manual == 2:
            # Basic demo
            fields[0o00] = "{trigAb}500m{acV}{bw}"
            fields[0o10] = ""

            fields[0o01] = " 50mV{Omega}"
            fields[0o11] = "{Omega} FAULT!"

            fields[0o02] = " 2A"
            fields[0o12] = ""

            fields[0o03] = " 200mV{gnd}"
            fields[0o13] = ""

            fields[0o04] = "Readout "
            fields[0o05] = "demo {:)} {<3}"
            fields[0o14] = "Hello, w"
            fields[0o15] = "orld!"

            fields[0o07] = "{rise}A 500µs"
            fields[0o17] = "{fall}B 10µs"

        elif page_manual == 3:
            # XY demo
            fields[0o00] = "{x}500mV"
            fields[0o10] = ""

            fields[0o01] = " 50mV{Omega}"
            fields[0o11] = ""

            fields[0o02] = ""
            fields[0o12] = ""

            fields[0o03] = ""
            fields[0o13] = ""

            fields[0o04] = "XY mode "
            fields[0o05] = "demo {:)} {<3}"
            fields[0o14] = "Hello, w"
            fields[0o15] = "orld!"

            fields[0o07] = " X-Y"
            fields[0o17] = ""

        if fields_enc is None:
            fields_enc = [charset.encode(i) for i in fields]
        field_enc = fields_enc[fld]

        if pos < len(field_enc):
            char = field_enc[pos]
        else:
            char = 0xFF

        return eeplut.decompose(
            char,
            "CHAR7", "CHAR6", "CHAR5", "CHAR4", "CHAR3", "CHAR2", "CHAR1", "CHAR0"
        )

