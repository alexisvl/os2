# OS-2 on-screen display

## Display layout

The display has several areas where on-screen display content is overlayed with
the primary signal display:

```
VERT1   VERT2   VERT3   VERT4


>------- trig  level -------<


DESC1DESC2DESC3         HORIZ
```

All text fields are eight spaces long.

- `VERT1` through `VERT4`: each is "owned" by a different vertical channel
module. It typically displays a scale factor (like "5V") as well as one
of a few icons (a sine wave sign to indicate AC coupling, a ground symbol
for ground coupling, a Tek-style halfwidth BW for bandwidth limiting,
trigger indicators, an ohm sign for 50-ohm termination, an alert sign
for termination overheat or other critical conditions, and a transient
"IDENT" indication when a probe identifier is activated).

- `DESC1` to `DESC3` are arranged so they flow together into a single field,
and exist so that an optional captioning plugin may be used to write user-
specified labels onto the display.

- `HORIZ` displays a horizontal scale factor (like "50µs") and alternative
trigger source icons ("AU" for auto triggering, "LN" for line triggering)
that are not associated with a vertical channel.

- "Trig level" is a dashed line that can be optionally drawn across the display
at the selected trigger point. (Suggested dashed-line implementation: 555
timer, astable, reset line driven by sweep gate. This will reset it on every
sweep so the dashes line up)

All vertical channels should emit a space (0x7F, no lines asserted) for
position 0. The trigger module will insert trigger level symbols on the
relevant channel during the scan sequence.

## Character encoding

The OS-2 character encoding is seven bits wide, and is defined by `charset.py`.
The characters are assigned to place the most important codepoints in the upper
half, meaning modules using only those can use a single six-bit open drain bus
driver.

## Binary encoding

Binary mode, selected by the Special module's assertion of the `BINARY#` signal,
emits information in a special binary coding that fits more data into the eight
character window.

When in binary mode, modules should emit data even when not enabled.

The Special module is responsible for re-rendering this data into human readable
text and editing the bus.

Each character uses all eight bits (with INTEN as the MSB), and are defined as
follows:

| Module type | Character | Value   | Description                               |
|-------------|-----------|---------|-------------------------------------------|
| Vertical    | 0         | 0x00    | First trigger active                      |
|             |           | 0x01    | Second trigger active                     |
|             |           | 0x02    | First trigger auto                        |
|             |           | 0x03    | Second trigger auto                       |
|             |           | 0x04    | First trigger roll                        |
|             |           | 0x05    | Second trigger roll                       |
|             |           | 0xFF    | Not triggered (same as non-binary)        |
|             | 4         | 0x00    | BIT FIELD 1: no vernier                   |
|             |           | 0x01    | BIT FIELD 1: vernier, less than indicated |
|             |           | 0x02    | BIT FIELD 1: vernier, more than indicated |
|             |           | 0x03    | BIT FIELD 1: uncal                        |
|             |           | 0x00    | BIT FIELD 2: ground coupling              |
|             |           | 0x04    | BIT FIELD 2: DC coupling                  |
|             |           | 0x08    | BIT FIELD 2: AC coupling                  |
|             |           | 0x0C    | BIT FIELD 2: (reserved)                   |
|             |           | 0x10    | BIT FLAG: terminated                      |
|             |           | 0x20    | BIT FLAG: bandwidth limited               |
|             |           | 0x40    | BIT FLAG: inverted                        |
|             |           | 0x80    | BIT FLAG: identify active                 |
| Horizontal  | 0         | 0x-0    | BIT FIELD 1: trigger 1 DC coupled         |
|             |           | 0x-1    | BIT FIELD 1: trigger 1 AC coupled         |
|             |           | 0x-2    | BIT FIELD 1: trigger 1 low freq reject    |
|             |           | 0x-3    | BIT FIELD 1: trigger 1 high freq reject   |
|             |           | 0x-4    | BIT FIELD 1: trigger 1 noise reject       |
|             |           | 0x-5    | BIT FIELD 1: trigger 1 continuous roll    |
|             |           | 0x-F    | BIT FIELD 1: XY mode, no trigger          |
|             |           | 0x0-    | BIT FIELD 2: trigger 2 DC coupled         |
|             |           | 0x1-    | BIT FIELD 2: trigger 2 AC coupled         |
|             |           | 0x2-    | BIT FIELD 2: trigger 2 low freq reject    |
|             |           | 0x3-    | BIT FIELD 2: trigger 2 high freq reject   |
|             |           | 0x4-    | BIT FIELD 2: trigger 2 noise reject       |
|             |           | 0x5-    | BIT FIELD 2: trigger 2 continuous roll    |
|             |           | 0xF-    | BIT FIELD 2: trigger 2 does not exist     |
|             | 4         | 0x00    | BIT FIELD 1: trigger 1 no vernier         |
|             |           | 0x01    | BIT FIELD 1: t1 vernier, < indicated      |
|             |           | 0x02    | BIT FIELD 1: t1 vernier, > indicated      |
|             |           | 0x03    | BIT FIELD 1: t1 uncal                     |
|             |           | 0x00    | BIT FIELD 2: trigger 1 norm               |
|             |           | 0x04    | BIT FIELD 2: trigger 1 auto               |
|             |           | 0x08    | BIT FIELD 2: trigger 1 single seq         |
|             |           | 0x0C    | BIT FIELD 2: trigger 1 other              |
|             |           | 0x00    | BIT FIELD 3: trigger 2 no vernier         |
|             |           | 0x10    | BIT FIELD 3: t2 vernier, < indicated      |
|             |           | 0x20    | BIT FIELD 3: t2 vernier, > indicated      |
|             |           | 0x30    | BIT FIELD 3: t2 uncal                     |
|             |           | 0x40    | BIT FIELD 4: trigger 2 run after delay    |
|             |           | 0x80    | BIT FIELD 4: trigger 2 trig after delay   |
|             | 5         | 0x00    | BIT FIELD 0: only trigger 1 displayed     |
|             |           | 0x01    | BIT FIELD 0: only trigger 2 displayed     |
|             |           | 0x02    | BIT FIELD 0: mixed display                |
|             |           | 0x03    | BIT FIELD 0: both display                 |
|             |           | 0x04    | BIT FLAG: line trigger                    |
|             |           | 0x10    | BIT FLAG: trigger 1 slope falling         |
|             |           | 0x20    | BIT FLAG: trigger 2 slope falling         |
|             |           | 0x40    | BIT FLAG: untriggered roll in auto        |
|             |           | 0x80    | BIT FLAG: trigger source external         |
| Both        | 1         | 0xXX    | XX is coefficient of scale/div            |
|             | 2         | 0xXX    | XX is magnitude of scale/div (2s comp)    |
|             | 3         | 0x00    | Unit: unitless                            |
|             |           | 0x01    | Unit: V                                   |
|             |           | 0x02    | Unit: A                                   |
|             |           | 0x03    | Unit: Ω                                   |
|             |           | 0x04    | Unit: S (mho)                             |
|             |           | 0x05    | Unit: F                                   |
|             |           | 0x06    | Unit: H                                   |
|             |           | 0x07    | Unit: s                                   |
|             |           | 0x08    | Unit: Hz                                  |
|             |           | 0x09    | Unit: dBm                                 |
|             |           | 0x0A    | Unit: dBc                                 |
|             |           | 0x0B    | Unit: dB                                  |
|             |           | 0x0C    | Unit: V/rtHz                              |
|             |           | 0x0D    | Unit: A/rtHz                              |
| Both        | 6         | 0x00    | Module ID: OSVA (basic vert amp)          |
|             |           | 0x70    | Module ID: OSHS (basic single sweep)      |
|             |           | 0x71    | Module ID: OSDS (dual sweep)              |
|             |           | 0xFE    | Module not known or not specified         |
|             |           | 0xFF    | Module not present
|             | 7         | 0x01    | BIT FLAG: fault (invalid configuration)   |
|             |           | 0x02    | BIT FLAG: fault (input overload)          |
|             |           | 0x04    | BIT FLAG: fault (needs calibration)       |
|             |           | 0x08    | BIT FLAG: fault (warming up)              |
|             |           | 0x10    | BIT FLAG: no data (waiting/acquiring)     |
|             |           | 0x80    | BIT FLAG: module disabled                 |
