// CRT front
translate([-96.5/2,-84/2,0]) {
    intersection(){
    translate([96.5/2,84/2,75])
        cylinder(h=25,r1=96.5/2/0.707,r2=40);
    cube(size=[96.5,84,100]);
    }
    cube(size=[96.5,84,75]);
    color("brown")
        translate([96.5/2, 84/2, ])
        rotate([0, 180, 0])
        text("11ЛО2И", halign="center", valign="center");
}

// Anode cap
color("black")
translate([-2-96.5/2, 0, 75])
rotate([0, 90, 0])
cylinder(h=2,r=5);

// Middle taper
translate([0,0,100])
cylinder(
 h=135,
 r1=40,
 r2=25
);

// Rear section
translate([0, 0, 235])
    cylinder(h=155, r=25);

// Connector
translate([0, 0, 390])
    cylinder(h=20, r=37/2);
    
// Deflection pins
color("black") {
translate([-5, 20, 260])
    rotate([-90, 0, 0])
    cylinder(h=10, r=0.5);
translate([5, 20, 260])
    rotate([-90, 0, 0])
    cylinder(h=10, r=0.5);
translate([20, -5, 260])
    rotate([0, 90, 0])
    cylinder(h=10, r=0.5);
translate([20, 5, 260])
    rotate([0, 90, 0])
    cylinder(h=10, r=0.5);
}

// Front panel
difference() {
    translate([-88, -60, -2])
        cube([149, 120, 1]);
    // CRT cutout
    translate([-105/2, -92/2, -2.02])
        cube([105, 92, 1.04]);
}

// Rods
translate([-48, -51, 0])
    cylinder(h=430, r=3.175);
translate([48, -51, 0])
    cylinder(h=430, r=3.175);
translate([-48, 51, 0])
    cylinder(h=430, r=3.175);
translate([48, 51, 0])
    cylinder(h=430, r=3.175);

// PCB-007-00
translate([-88, -100/2, 430-160])
union(){
    cube([1.6, 100, 160]);
    color("brown")
        translate([1.7,100/2,160/2])
        rotate([0, 90, 0])
        text("PCB-007-00", halign="center");
}

// PCB-008-00
// Positioned 5mm rightward from centered
translate([-125/2 - 5, -43.8, 430-160])
union(){
    cube([125, 1.6, 160]);
    color("brown")
        translate([125/2, 1.6, 80])
        rotate([0, -270, 90])
        text("PCB-008-00", halign="center", valign="center");
}

// PCB-008-00 clearance measurement
/*
translate([-5, -43+1.6, 350])
    color("orange")
    cube([10, 16.1, 10]);

translate([-20, -43+1.6, 350])
    color("orange")
    cube([10, 18, 10]);
*/