// CRT front
translate([-96.5/2,-84/2,0]) {
    intersection(){
    translate([96.5/2,84/2,75])
        cylinder(h=25,r1=96.5/2/0.707,r2=40);
    cube(size=[96.5,84,100]);
    }
    cube(size=[96.5,84,75]);
    color("brown")
        translate([96.5/2, 84/2, ])
        rotate([0, 180, 0])
        text("11ЛО2И", halign="center", valign="center");
}

// Anode cap
color("black")
translate([-2-96.5/2, 0, 75])
rotate([0, 90, 0])
cylinder(h=2,r=5);

// Middle taper
translate([0,0,100])
cylinder(
 h=135,
 r1=40,
 r2=25
);

// Rear section
translate([0, 0, 235])
    cylinder(h=155, r=25);

// Connector
translate([0, 0, 390])
    cylinder(h=20, r=37/2);
    
// Deflection pins
color("black") {
translate([-5, 20, 260])
    rotate([-90, 0, 0])
    cylinder(h=10, r=0.5);
translate([5, 20, 260])
    rotate([-90, 0, 0])
    cylinder(h=10, r=0.5);
translate([20, -5, 260])
    rotate([0, 90, 0])
    cylinder(h=10, r=0.5);
translate([20, 5, 260])
    rotate([0, 90, 0])
    cylinder(h=10, r=0.5);
}
